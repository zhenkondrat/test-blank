<?php namespace Test\Blank\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('test_blank_comments', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->text('content')->nullable()->default(null);
            $table->string('attach')->nullable()->default(null);
            $table->integer('product_id')->unsigned()->nullable()->default(0);
            $table->string('file_path')->nullable()->default(null);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('test_blank_comments');
    }
}
