<?php namespace Test\Blank\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('test_blank_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->decimal('price', 10, 2)->nullable()->default(null);
            $table->integer('rate')->unsigned()->nullable()->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('test_blank_products');
    }
}
