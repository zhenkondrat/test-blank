<?php namespace Test\Blank\Components;

use Test\Blank\Models\Product;
use Cms\Classes\ComponentBase;

class Products extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Products Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getProducts()
    {
        return Product::orderBy('id', 'desc')->get();
    }
}
