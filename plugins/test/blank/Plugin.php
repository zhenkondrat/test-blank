<?php namespace Test\Blank;

/**
 * The plugin.php file (called the plugin initialization script) defines the plugin information class.
 */

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Test Blank',
            'description' => 'Blank for test job.',
            'icon'        => 'icon-puzzle-piece'
        ];
    }

    public function registerComponents()
    {
        return [
            '\Test\Blank\Components\Todo' => 'todo',
            '\Test\Blank\Components\Products' => 'catalog',
            '\Test\Blank\Components\Product' => 'product',
            '\Test\Blank\Components\Comments' => 'comments'

        ];
    }

   public function registerNavigation()
    {
        return [
            'catalog' => [
                'label'       => 'Каталог',
                'url'         => \Backend::url('Test/Blank/products'),
                'icon'        => 'icon-list-alt',
                'order'       => 500,
                'sideMenu' => [
                    'products' => [
                        'label'       => 'Товары',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('Test/Blank/products'),
                    ],           
                ]

            ],
             'comment' => [
                'label'       => 'Відгуки',
                'url'         => \Backend::url('Test/Blank/comments'),
                'icon'        => 'icon-list',
                'order'       => 500,
                'sideMenu' => [
                    'products' => [
                        'label'       => 'Комент',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('Test/Blank/comments'),
                    ],           
                ]

            ]
        ];
    }
}
