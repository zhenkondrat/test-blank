<?php namespace Test\Blank\Models;

use Model;
use Form;
use Db;

/**
 * Comment Model
 */
class Comment extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'test_blank_comments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'email',
        'content',
        'attach'
    ];

    public function listStatuses($fieldName, $value, $formData)
        {
            $myArray = Db::table('test_blank_products')->get();
            $attributes = array_combine($myArray->lists('id'), $myArray->lists('name'));
            return $attributes;
        }

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = ['product' => 'Test\Blank\Models\Product', 'key' => 'product_id', 'otherKey' => 'id'];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['attach' => ['System\Models\File']];
    // public $attachOne = [];
    public $attachMany = [];
}
